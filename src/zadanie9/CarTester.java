package zadanie9;

public class CarTester {

    public static void main(String[] args) {

        Car car = new Car("Volvo", 4, 100, 201, 100, 100, 2000);
        System.out.println(car.getCarName());

        System.out.println("Liczba pasazerow: " + car.liczbaPasazerow);

        car.addPassenger();
        System.out.println("Liczba pasazerow: " + car.liczbaPasazerow);
        car.addPassenger();
        System.out.println("Liczba pasazerow: " + car.liczbaPasazerow);
        car.addPassenger();
        System.out.println("Liczba pasazerow: " + car.liczbaPasazerow);
        car.addPassenger();
        System.out.println("Liczba pasazerow: " + car.liczbaPasazerow);
        car.addPassenger();
        System.out.println("Liczba pasazerow: " + car.liczbaPasazerow);
        car.removePassenger();
        car.removePassenger();
        car.removePassenger();
        car.removePassenger();
        System.out.println("Liczba pasazerow: " + car.liczbaPasazerow);
        car.removePassenger();
        System.out.println("Liczba pasazerow: " + car.liczbaPasazerow);
        System.out.println("Obecna predkosc: " + car.obecnaPredkosc);
        car.speedIncrease(20);
        System.out.println("Obecna predkosc: " + car.obecnaPredkosc);
        car.addPassenger();
        car.speedIncrease(20);
        System.out.println("Obecna predkosc: " + car.obecnaPredkosc);
        car.speedIncrease(-100);
        System.out.println("Obecna predkosc: " + car.obecnaPredkosc);
        car.removePassenger();
        System.out.println("Obecna predkosc: " + car.obecnaPredkosc);
        car.speedDecrease(-100);
        System.out.println("Obecna predkosc: " + car.obecnaPredkosc);
        car.addPassenger();
        car.speedDecrease(-100);
        System.out.println("Obecna predkosc: " + car.obecnaPredkosc);
        car.speedDecrease(121);
        System.out.println("Obecna predkosc: " + car.obecnaPredkosc);
    }
}
