package zadanie9;

public class Car {

    private String marka;
    private int iloscSiedzen;
    private int pojemnoscSilnika;
    private int iloscKM;
    public int obecnaPredkosc;
    private int obecnyPrzebieg;
    private int rokProdukcji;
    public int liczbaPasazerow = 0;
    private int speedIncrease;
    private int speedDecrease;
    private double maxPredkosc;

    public Car(String marka, int iloscSiedzen, int pojemnoscSilnika, int iloscKM, int obecnaPredkosc, int obecnyPrzebieg, int rokProdukcji) {
        this.marka = marka;
        this.iloscSiedzen = iloscSiedzen;
        this.pojemnoscSilnika = pojemnoscSilnika;
        this.iloscKM = iloscKM;
        this.obecnaPredkosc = obecnaPredkosc;
        this.obecnyPrzebieg = obecnyPrzebieg;
        this.rokProdukcji = rokProdukcji;
    }

    public String getCarName() {
        return marka + " " + rokProdukcji;
    }

    public void addPassenger() {
        try {
            if (iloscSiedzen == liczbaPasazerow) throw new IllegalArgumentException();
            else {
                liczbaPasazerow += 1;
                System.out.println("Dodales pasazera.");
            }
            ;

        } catch (IllegalArgumentException e) {
            System.out.println(e + " brak miejsc, nie mozesz dodac kolejnego pasazera");
        }
    }

    public void removePassenger() {
        try {
            if (liczbaPasazerow == 0) throw new IllegalArgumentException();
            else {
                liczbaPasazerow -= 1;
                System.out.println("Usunales pasazera");
            }
            ;

        } catch (IllegalArgumentException e) {
            System.out.println(e + " nikogo juz nie ma");
        }
    }

    public int speedIncrease(int speedIncrease) {
        try {
            if (liczbaPasazerow == 0) throw new IllegalArgumentException();

            try {

                if (speedIncrease < 0) throw new IllegalArgumentException();

                try {

                    if (iloscKM > 200) {
                        maxPredkosc = 1.0 * iloscKM;

                        if ((1.0 * iloscKM) < (obecnaPredkosc + speedIncrease)) throw new IllegalArgumentException();
                        else {
                            obecnaPredkosc += speedIncrease;
                        }

                    } else {
                        maxPredkosc = 1.2 * iloscKM;
                        if ((1.2 * iloscKM) < (obecnaPredkosc + speedIncrease)) throw new IllegalArgumentException();
                        else {
                            obecnaPredkosc += speedIncrease;
                        }
                    }

                } catch (IllegalArgumentException e) {
                    System.out.println(e + " Chciales przyspieszyc o " + speedIncrease + ". Twoja ilosc KM(" + iloscKM + ") pozwala na jechanie z maksymalna predkoscia: " + maxPredkosc);
                }

            } catch (IllegalArgumentException e) {
                System.out.println(e + " Nie mozesz zwiekszyc predkosci o wartosc: " + speedIncrease + " podaj wartosc dodatnia");

            }
        } catch (IllegalArgumentException e) {
            System.out.println(e + " Brakuje kierowcy !");

        }
        return obecnaPredkosc;
    }

    public int speedDecrease(int speedDecrease) {

        try {

            if (liczbaPasazerow == 0) throw new IllegalArgumentException();

            try {

                if (speedDecrease < 0) throw new IllegalArgumentException();

                try {

                    if (0 > obecnaPredkosc - speedDecrease) throw new IllegalArgumentException();

                    else {
                        obecnaPredkosc -= speedDecrease;
                    }


                } catch (IllegalArgumentException e) {
                    System.out.println(e + " Nie mozesz jechac ponizej 0 ! Mozesz maksymalnie zmniejszyc predkosc o: " + obecnaPredkosc);
                }

            } catch (IllegalArgumentException e) {
                System.out.println(e + " Nie mozesz zmniejszyc predkosci o wartosc: " + speedDecrease + " podaj wartosc dodatnia");
            }


        } catch (IllegalArgumentException e) {
            System.out.println(e + " Brakuje kierowcy !");

        }
        return obecnaPredkosc;
    }
}
