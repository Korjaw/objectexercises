package zadanie5;

public class Prostokat {

    private int a;
    private int b;

    public Prostokat(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int obliczObwod(int a, int b) {
        int obwod = 2*a+2*b;
        return obwod;
    }

    public int obliczPole(int a, int b) {
        int pole = a*b;
        return pole;
    }

    @Override
    public String toString() {
        return "Prostokat{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }
}
