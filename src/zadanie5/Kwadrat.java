package zadanie5;

public class Kwadrat {

    private int dlugoscKrawedzi;

    public Kwadrat(int dlugoscKrawedzi) {
        this.dlugoscKrawedzi = dlugoscKrawedzi;
    }

    public int obliczObwod(int dlugoscKrawedzi) {
        int obwod = 4*dlugoscKrawedzi;
        return obwod;
    }

    public int obliczPole(int dlugoscKrawedzi) {
        int pole = dlugoscKrawedzi*dlugoscKrawedzi;
       return pole;
    }

    @Override
    public String toString() {
        return "Kwadrat{" +
                "dlugoscKrawedzi=" + dlugoscKrawedzi +
                '}';
    }
}
