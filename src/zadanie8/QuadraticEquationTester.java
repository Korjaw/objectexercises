package zadanie8;

public class QuadraticEquationTester {

    public static void main(String[] args) {

        QuadraticEquation liczby = new QuadraticEquation(1, -4, -5);

        try {

            if (liczby.a == 0) throw new IllegalArgumentException();

            try {

                System.out.println("delta: " + liczby.calculateDelta());
                if (liczby.calculateDelta() < 0) throw new DeltaLessThanZeroException();
                else if (liczby.calculateDelta() == 0) {
                    System.out.println("x0: " + liczby.calculateX0());
                } else {
                    System.out.println("x1 : " + liczby.calculateX1());
                    System.out.println("x2 : " + liczby.calculateX2());

                }
            } catch (IllegalArgumentException e) {
                System.out.println(e + " dla ujemnej delty nie ma pierwiastkow");
            }

        } catch (IllegalArgumentException e) {
            System.out.println(e + " Parametr 'a' nie moze byc rowny 0 !");
        }
    }
}
