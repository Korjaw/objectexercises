package zadanie8;

public class QuadraticEquation {

    public double a;
    private double b;
    private double c;
    private double delta;
    private double x1;
    private double x2;
    private double x0;
    private double pierwiastek;

    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double calculateDelta() {
        delta = (b * b) - (4 * a * c);
        return delta;
    }

    public double calculateX1() {
        pierwiastek = Math.sqrt(delta);
        x1 = (-b-pierwiastek)/(2*a);
        return x1;
    }

    public double calculateX2() {
        pierwiastek = Math.sqrt(delta);
        x2 = (-b+pierwiastek)/(2*a);
        return x2;
    }

    public double calculateX0() {
        x0 = -b/(2*a);
        return x0;
    }
}
