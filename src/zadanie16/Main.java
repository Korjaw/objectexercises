package zadanie16;

public class Main {

    public static void main(String[] args) {
        Computer computer = new Computer();

        System.out.println(computer);

        computer.ustawKarteGraficzna("GeForce");
        System.out.println(computer);

        computer.setMocProcesora(2);
        computer.setNazwaKomputera("Default");
        computer.setRozmiarDysku(100);
        computer.setWielkoscRAM(4);
        System.out.println(computer);
        System.out.println(computer.czyJestKomputeremDoGier());


    }
}
