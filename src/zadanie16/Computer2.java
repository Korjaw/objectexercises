package zadanie16;

public class Computer2 {
    private String name;
    private double cpuPower;
    private int diskSpace;
    private int ram;
    private String gpuName;

    public boolean isGamingPC() {
        // która zwraca true jeśli przelicznik mocy obliczeniowej z powyższej metody > 10
//        if (getProcessingPower() > 10) {
//            return true;
//        } else {
//            // która zwraca false jeśli przelicznik jest niższy niż 10.
//            return false;
//        }
        return getProcessingPower() > 10;
    }

    //stwórz metodę 'podaj moc obliczeniową' która zwraca
    public double getProcessingPower() {
        if (hasGPU()) { //(moc_procesora * 1.45 + (0.25 * ilość_pam_ram))  - jeśli jest karta graficzna
            return (cpuPower * 1.45) + (0.25 * ram);
        } else { //(moc_procesora * 0.95 + (0.3 * ilość_pam_ram))  - jeśli nie ma karty graficznej
            return (cpuPower * 0.95) + (0.3 * ram);
        }
    }

    // stwórz metodę 'ustaw kartę graficzną' która ustawia pole
    // 'nazwa karty graficznej' na jakąś wartość podaną w parametrze.
    public void setGPUName(String name) {
        gpuName = name;
    }

    // stwórz metodę 'czy posiada kartę graficzną' która zwraca true
    // jeśli nazwa karty graficznej jest różna od null, lub false
    // jeśli jest równa null.
    public boolean hasGPU() {
        // jeśli jest różne to zwróci true
        return gpuName != null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCpuPower() {
        return cpuPower;
    }

    public void setCpuPower(double cpuPower) {
        this.cpuPower = cpuPower;
    }

    public int getDiskSpace() {
        return diskSpace;
    }

    public void setDiskSpace(int diskSpace) {
        this.diskSpace = diskSpace;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }
}