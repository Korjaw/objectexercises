package zadanie16;

public class Computer {

    private String nazwaKomputera;
    private float mocProcesora;
    private int rozmiarDysku;
    private int wielkoscRAM;
    private String nazwaKartyGraficznej;

    public Computer() {
    }

    public void ustawKarteGraficzna(String nazwaKartyGraficznej) {
        this.nazwaKartyGraficznej = nazwaKartyGraficznej;
    }

    public Boolean czyPosiadaKarteGraficzna() {

        if(nazwaKartyGraficznej.equals("null")){
            return false;
        } else {
            return true;
        }
    }

    public double podajMocObliczeniowa() {

        if (czyPosiadaKarteGraficzna()==true) {

            return (getMocProcesora()*1.45+(0.25*getWielkoscRAM()));


        } else {
            return (getMocProcesora()*0.95+(0.3*getWielkoscRAM()));
        }
    }

    public Boolean czyJestKomputeremDoGier() {

        if (podajMocObliczeniowa()>10) {
            return true;
        } else {
            return false;
        }
    }

    public String getNazwaKomputera() {
        return nazwaKomputera;
    }

    public void setNazwaKomputera(String nazwaKomputera) {
        this.nazwaKomputera = nazwaKomputera;
    }

    public float getMocProcesora() {
        return mocProcesora;
    }

    public void setMocProcesora(float mocProcesora) {
        this.mocProcesora = mocProcesora;
    }

    public int getRozmiarDysku() {
        return rozmiarDysku;
    }

    public void setRozmiarDysku(int rozmiarDysku) {
        this.rozmiarDysku = rozmiarDysku;
    }

    public int getWielkoscRAM() {
        return wielkoscRAM;
    }

    public void setWielkoscRAM(int wielkoscRAM) {
        this.wielkoscRAM = wielkoscRAM;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "nazwaKomputera='" + getNazwaKomputera() + '\'' +
                ", mocProcesora=" + getMocProcesora() +
                ", rozmiarDysku=" + getRozmiarDysku() +
                ", wielkoscRAM=" + getWielkoscRAM() +
                ", nazwaKartyGraficznej='" + nazwaKartyGraficznej + '\'' +
                '}';
    }
}
