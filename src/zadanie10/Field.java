package zadanie10;


public class Field {

    private int x ;
    private int y ;

    public boolean[][] pole;

    public Field(int x, int y) {
        this.pole = new boolean[x][y]; // nadaje wymiary x i y planszy
        this.x = x;
        this.y = y;
    }


    public void printField() { // wyswietla plansze

        for (int i = 0; i < getX(); i++) {
            System.out.println("");
            for (int j = 0; j < getY(); j++) {
                if (pole[i][j] == false) {
                    System.out.print("O");
                } else if (pole[i][j] == true) {
                    System.out.print("X");
                }
            }

        }
        System.out.println("");
    }

    public void checkCell(int x, int y) { // sprawdzamy pole
        if (pole[x][y] == false) { // jesli nie bylo sprawdzane
            pole[x][y] = true;
        } else if (pole[x][y] == true) { // jesli bylo sprawdzane
            System.out.println("komorka byla sprawdzana");
        }

    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}