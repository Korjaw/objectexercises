package zadanie10;

import java.util.Scanner;

public class Main {

//    private int x2;
//    private int y2;
//
//    public Main(int x2, int y2) {
//        this.x2 = x2;
//        this.y2 = y2;
//    }
//
//    public int getX2() {
//        return x2;
//    }
//
//    public int getY2() {
//        return y2;
//    }

    public static void main(String[] args) {

        Field field = new Field(10, 10);
        String liniaLiczbaX;
        String liniaLiczbaY;
        String metoda;

        Scanner wejscie = new Scanner(System.in);
        System.out.println("Podaj komende (printfield, checkcell) ");

        while (wejscie.hasNextLine()) {
            try {
                metoda = wejscie.nextLine();

                if (metoda.equals("checkcell")) {
                    System.out.println("Podaj wspolrzedna X ");
                    liniaLiczbaX = wejscie.nextLine();
                    System.out.println("Podaj wspolrzedna Y ");
                    liniaLiczbaY = wejscie.nextLine();

                    try {
                        int xDoSpr = Integer.parseInt(liniaLiczbaX);
                        int yDoSpr = Integer.parseInt(liniaLiczbaY);

                        if ((xDoSpr >= field.getX()) || (yDoSpr >= field.getY()))
                            throw new IllegalArgumentException();
                        else {
                            field.checkCell(xDoSpr, yDoSpr);
                        }
                    } catch (IllegalArgumentException e) {
                        System.err.println("Blad");
//                        System.out.println(e + " Bledne wspolprzedne, maksymalna wspolrzedna to: " + (field.getX() - 1) + "x" + (field.getY() - 1) + ", a podales: " + field.main.x2 + "x" + field.main.y2);
                    }
                } else if (metoda.equals("printfield")) {
                    field.printField();
                } else throw new IllegalArgumentException();

            } catch (IllegalArgumentException e) {
                System.out.println(e + " Nieznana metoda");
            }


        }
    }
}