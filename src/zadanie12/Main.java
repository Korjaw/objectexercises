package zadanie12;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String metoda;
        String figura;
        String liczbaX;
        String liczbaY;
        Scanner wejscie = new Scanner(System.in);
        System.out.println("Podaj metode");

        while (wejscie.hasNextLine()) {

            try {
                metoda = wejscie.nextLine();

                if ((metoda.equals("pole")) || (metoda.equals("obwod"))) {


                    try {
                        System.out.println("Podaj figure");
                        figura = wejscie.nextLine();

                        if (figura.equals("kwadrat") || (figura.equals("kolo")) || (figura.equals("prostokat"))) {

                            System.out.println("Podaj X");
                            liczbaX = wejscie.nextLine();
                            int X = Integer.parseInt(liczbaX);

                            if (figura.equals("kwadrat")) {
                                Kwadrat kwadrat = new Kwadrat(X);
                                if (metoda.equals("pole")) {
                                    System.out.println(kwadrat.obliczPole(X));
                                    ;
                                }

                                if (metoda.equals("obwod")) {
                                    System.out.println(kwadrat.obliczObwod(X));
                                }

                            } else if ((figura.equals("prostokat"))) {
                                System.out.println("Podaj Y");
                                liczbaY = wejscie.next();
                                int Y = Integer.parseInt(liczbaY);
                                Prostokat prostokat = new Prostokat(X, Y);

                                if (metoda.equals("pole")) {
                                    System.out.println(prostokat.obliczPole(X, Y));
                                }

                                if (metoda.equals("obwod")) {
                                    System.out.println(prostokat.obliczObwod(X, Y));
                                }

                            } else if (figura.equals("kolo")) {
                                Kolo kolo = new Kolo(X);
                                if (metoda.equals("pole")) {
                                    System.out.println(kolo.obliczPole(X));
                                }

                                if (metoda.equals("obwod")) {
                                    System.out.println(kolo.obliczObwod(X));
                                }
                            }

                        } else throw new IllegalArgumentException();

                    } catch (IllegalArgumentException e) {
                        System.out.println(e + " Brak takiej figury, dostepne figury to: kwadrat,kolo,prostokat");
                    }


                } else throw new IllegalArgumentException();

            } catch (IllegalArgumentException e) {
                System.out.println(e + " Brak takiej opcji, mozliwe opcje to: pole, obwod");
            }
        }
    }
}