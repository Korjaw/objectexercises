package zadanie12;

public class Kolo {

    private int promien;

    public Kolo(int promien) {
        this.promien = promien;
    }

    public double obliczObwod (int promien) {
        double obwod = 2*3.14*promien;
        return obwod;
    }

    public double obliczPole (int promien) {
        double pole = 3.14*promien*promien;
        return pole;
    }

}
