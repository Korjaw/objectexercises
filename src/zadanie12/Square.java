package zadanie12;

public class Square {
    private double a;

    public Square(double a) {
        this.a = a;
    }

    public double obliczObwod() {
        return 4 * a;
    }

    public double obliczPole() {
        return a * a;
    }
}