package zadanie12;

public class Rectangle {
    private double a, b;

    public Rectangle(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public double obliczObwod() {
        return (2 * a) + (2 * b);
    }

    public double obliczPole() {
        return a * b;
    }
}