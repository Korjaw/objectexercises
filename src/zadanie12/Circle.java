package zadanie12;

public class Circle {
    private double r;

    public Circle(double r) {
        this.r = r;
    }

    public double obliczObwod() {
        return 2 * Math.PI * r;
    }

    public double obliczPole() {
        return Math.PI * r * r;
    }
}