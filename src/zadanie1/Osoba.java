package zadanie1;

public class Osoba {

    private String imie;
    private int wiek;

    public Osoba(String imie, int wiek) {
        this.imie = imie;
        this.wiek = wiek;
    }

    public void printYourNameAndAge() {
        System.out.println(getImie() + getWiek());
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }

    @Override
    public String toString() {
        return "Jestem " + getImie() + ", mam " + getWiek() + " lat.";
    }

    public static void main(String[] args) {
        Osoba osoba = new Osoba("jan", 1);
        Osoba osoba2 = new Osoba("janusz", 11);
        Osoba osoba3 = new Osoba("maria", 111);

        System.out.println(osoba);
        System.out.println(osoba2);
        System.out.println(osoba3);

    }
}
