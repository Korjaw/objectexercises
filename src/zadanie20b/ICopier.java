package zadanie20b;

public interface ICopier {
    String copy(String something);
}