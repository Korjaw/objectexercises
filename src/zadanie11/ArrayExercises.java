package zadanie11;

import java.util.ArrayList;

public class ArrayExercises {



    public ArrayExercises() {
    }

    public static float suma(ArrayList<Integer> list) {

        float sum = 0;
        for (int i = 0; i < list.size(); i++) {
            sum += list.get(i);
        }

        return sum;
    }

    public static int iloczyn(ArrayList<Integer> list) {

        int iloczyn = 1;
        for (int i = 0; i < list.size(); i++) {
            iloczyn *= list.get(i);
        }

        return iloczyn;

    }

    public static float srednia(ArrayList<Integer> list) {
        float srednia = suma(list)/list.size();
        return srednia;
    }

    public static void negacja(ArrayList<Boolean> list) {
        for (int i=0; i<list.size();i++) {
            if (list.get(i)==true) {
                list.remove(i);
                list.add(i,false);
            } else {
                list.remove(i);
                list.add(i,true);
            }
            System.out.println(list.get(i));
        }

    }
}
