package zadanie11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Zadanie11_list_n_table {
    public static void main(String[] args) {
        List<Integer> lista = new ArrayList<>();
        lista.add(1);
        lista.add(5);
        lista.add(8);
        lista.add(4);
        lista.add(12);
        lista.add(3);

        List<Integer> kopia1 = copyList1(lista);
        List<Integer> kopia2 = copyList1(lista);
        List<Integer> kopia3 = copyList1(lista);
//        List<Integer> kopia4 = copyList1(lista);
//
//        List<Integer> jedna = copyVarNumberOfListsToList(kopia1, kopia4, kopia3);
        List<String> lista1 = new ArrayList<>(Arrays.asList("a ", "b", "c", "d", "e"));
        List<String> lista2 = new ArrayList<>(Arrays.asList("aa ", "bbb", "cb", "bd", "eddd"));
        List<String> lista3 = new ArrayList<>(Arrays.asList("aa ", "bb", "cc", "dcc", "ccce"));
        List<String> listGenericRewrite = genericCopyVarNumberOfListsToList();

    }

    /**
     * Kopiuje liste (tworzy nową instancję listy z tymi samymi elementami).
     *
     * @param listToCopy - lista do skopiowania.
     * @return - nowa, skopiowana lista.
     */
    public static List<Integer> copyList(List<Integer> listToCopy) {
        List<Integer> newList = new ArrayList<>();
        for (Integer value : listToCopy) {
            newList.add(value);
        }
        return newList;
    }

    /**
     * Kopiuje liste (tworzy nową instancję listy z tymi samymi elementami).
     *
     * @param listToCopy - lista do skopiowania.
     * @return - nowa, skopiowana lista.
     */
    public static List<Integer> copyList1(List<Integer> listToCopy) {
        List<Integer> newList = new ArrayList<>();
        newList.addAll(listToCopy);
        return newList;
    }

    /**
     * Kopiuje liste (tworzy nową instancję listy z tymi samymi elementami).
     *
     * @param listToCopy - lista do skopiowania.
     * @return - nowa, skopiowana lista.
     */
    public static List<Integer> copyList2(List<Integer> listToCopy) {
        List<Integer> newList = new ArrayList<>(listToCopy);
        return newList;
    }

    /**
     * Kopiuje elementy listy do tablicy.
     *
     * @param listToCopy - lista do skopiowania
     * @return tablica z elementami listy.
     */
    public static Integer[] copyListToTable(List<Integer> listToCopy) {
        Integer[] tablica = new Integer[listToCopy.size()];
        for (int i = 0; i < listToCopy.size(); i++) {
            tablica[i] = listToCopy.get(i);
        }
        return tablica;
    }

    /**
     * Kopiuje elementy listy do tablicy.
     *
     * @param listToCopy - lista do skopiowania
     * @return tablica z elementami listy.
     */
    public static Integer[] copyListToTable2(List<Integer> listToCopy) {
        Integer[] tablica = new Integer[listToCopy.size()];

        return listToCopy.toArray(tablica);
    }

    /**
     * Kopiuje elementy tablicy do listy.
     *
     * @param tableToCopy - tablica do skopiowania.
     * @return lista z elementami tablicy.
     */
    public static List<Integer> copyTableToList(Integer[] tableToCopy) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < tableToCopy.length; i++) {
            list.add(tableToCopy[i]);
        }
        return list;
    }

    /**
     * Kopiuje elementy tablicy do listy.
     *
     * @param tableToCopy - tablica do skopiowania.
     * @return lista z elementami tablicy.
     */
    public static List<Integer> copyTableToList2(Integer[] tableToCopy) {
        List<Integer> list = Arrays.asList(tableToCopy);
        return list;
    }

    /**
     * Tworzy nową listę z dwóch innych list
     *
     * @param list1 - lista pierwsza której elementy mają znaleźć się w nowej liście
     * @param list2 - lista druga której elementy mają znaleźć się w nowej liście
     * @return nowa lista z elementami listy list1 oraz list2
     */
    public static List<Integer> copyTwoListsToList(List<Integer> list1, List<Integer> list2) {
        List<Integer> list = new ArrayList<>();
        list.addAll(list1);
        list.addAll(list2);
        return list;
    }

    /**
     * Tworzy nową listę z dwóch innych list
     *
     * @param list1 - lista pierwsza której elementy mają znaleźć się w nowej liście
     * @param list2 - lista druga której elementy mają znaleźć się w nowej liście
     * @return nowa lista z elementami listy list1 oraz list2
     */
    public static List<Integer> copyTwoListsToList2(List<Integer> list1, List<Integer> list2) {
        List<Integer> list = new ArrayList<>(list1);
        list.addAll(list2);
        return list;
    }

    /**
     * Tworzy nową listę z wielu innych list.
     *
     * @param lists - listy do skopiowania.
     * @return listę z elementami wszystkich list przekazanych jako parametry.
     */
    public static List<Integer> copyVarNumberOfListsToList(List<Integer>... lists) {
        List<Integer> list = new ArrayList<>();
        for (List<Integer> listFromArguments : lists) {
            list.addAll(listFromArguments);
        }
        return list;
    }

    /**
     * Tworzy nową listę z wielu innych list.
     *
     * @param lists - listy do skopiowania.
     * @return listę z elementami wszystkich list przekazanych jako parametry.
     */
    public static List<Integer> copyVarNumberOfListsToList2(List<Integer>... lists) {
        List<Integer> list = new ArrayList<>();
        for (int i = 0; i < lists.length; i++) {
            list.addAll(lists[i]);
        }
        return list;
    }

    /**
     * Tworzy nową listę z wielu innych list.
     *
     * @param lists - listy do skopiowania.
     * @param <T>   - typ list.
     * @return listę z elementami wszystkich list przekazanych jako parametry.
     */
    public static <T> List<T> genericCopyVarNumberOfListsToList(List<T>... lists) {
        List<T> list = new ArrayList<>();
        for (int i = 0; i < lists.length; i++) {
            list.addAll(lists[i]);
        }
        return list;
    }

    /**
     * Porównuje ze sobą dwie listy (czy mają te same elementy).
     *
     * @param list1 - lista do porównania
     * @param list2 - lista do porównania
     * @param <T>   - typ list
     * @return wynik true jesli listy mają te same elementy, false jeśli nie.
     */
    public static <T> boolean genericCompareTwoLists(List<T> list1, List<T> list2) {
        if (list1.size() != list2.size()) {
            return false;
        }

        for (int i = 0; i < list1.size(); i++) {
            if (!list1.get(i).equals(list2.get(i))) { // jesli elementy sa rozne
                return false;
            }
        }
        // elementy sa rowne jesli przejdzie przez petle i nie zwroci zadnego false.
        return true;
        // to samo co list1.equals(list2)
    }

    /**
     * Porównuje ze sobą dwie listy (czy mają te same elementy).
     *
     * @param list1 - lista do porównania
     * @param list2 - lista do porównania
     * @return wynik true jesli listy mają te same elementy, false jeśli nie.
     */
    public static boolean compareTwoLists(List<Integer> list1, List<Integer> list2) {
        return list1.equals(list2);
    }
}