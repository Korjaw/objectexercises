package zadanie11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayTest {

    public ArrayTest() {
    }

    public static void kopia(ArrayList<Integer> list) {

        List<Integer> kopia = new ArrayList<>(list);
        System.out.println(kopia);
    }

    public static void identyczneWartosci(ArrayList<Integer> list) {
        int licznik = 0;
        int[] lista2 = {4,3,2,1,5,29,3,8};

        for (int i=0; i<list.size(); i++) {
            if (list.get(i)==lista2[i]) {
                licznik++;
            }
        }
        System.out.println(licznik);
    }

    public static void listaDoTablicy(ArrayList<Integer> list) {
        int[] tab = new int[list.size()];
        for (int i = 0; i<list.size(); i++) {
            tab[i] = list.get(i);
            System.out.print(tab[i] + " ");
        }

    }

    public static void tablicaDoListy(ArrayList<Integer> list) {
        Integer[] lista2 = {4,3,2,1,5,29,3,8};
        list = new ArrayList<Integer>(Arrays.asList(lista2));
        System.out.println(list);

    }

    public static void dwieListyDoJednej(ArrayList<Integer> list2, ArrayList<Integer> a, ArrayList<Integer> b) {
        list2.addAll(a);
        list2.addAll(b);
        System.out.println("2 listy w 1: " + list2);

    }

    public static void dowolnaIloscListDoJednej(ArrayList<Integer> list) {


    }

    public static void czyDwieTakieSame(ArrayList<Integer> list) {

    }



}
