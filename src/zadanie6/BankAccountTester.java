package zadanie6;

public class BankAccountTester {

    public static void main(String[] args) {

        BankAccount account = new BankAccount(0);
        System.out.println(account);

        account.addMoney(100);
        account.printBankAccountStatus();
        account.substractMoney(200);
        account.printBankAccountStatus();
    }
}
