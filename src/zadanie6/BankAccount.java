package zadanie6;

public class BankAccount {

    private int money;

    public BankAccount(int money) {
        this.money = money;
    }

    public void addMoney (int money) {
        this.money = this.money + money;
        System.out.println("Wplaciles: " + money );
    }

    public void substractMoney (int money) {
       this.money = this.money - money;
        System.out.println("Wyplaciles: " + money);
    }

    public void printBankAccountStatus() {
        System.out.println("Masz: " + money);
    }

    @Override
    public String toString() {
        return "BankAccount{" +
                "money=" + money +
                '}';
    }
}
