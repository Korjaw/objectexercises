package zadanie4;

import java.util.Scanner;

public class CalculatorTester {

    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        String liniaLiczba1;
        String liniaLiczba2;
        String metoda;
        Scanner wejscie = new Scanner(System.in);

        System.out.println("To jest kalkulator, bedziesz musial podac operacje, a nastepnie 2 liczby np. dodaj 20 20");

        while (wejscie.hasNextLine()) {
            metoda = wejscie.nextLine();
            liniaLiczba1 = wejscie.nextLine();
            liniaLiczba2 = wejscie.nextLine();

            try {

                int liczba1 = Integer.parseInt(liniaLiczba1);
                int liczba2 = Integer.parseInt(liniaLiczba2);

                try {

                    if (metoda.equals("dodaj")) {
                        System.out.println(calculator.addTwoNumbers(liczba1, liczba2));
                    } else if (metoda.equals("odejmij")) {
                        System.out.println(calculator.substractTwoNumbers(liczba1, liczba2));
                    } else if (metoda.equals("pomnoz")) {
                        System.out.println(calculator.multiplyTwoNumbers(liczba1, liczba2));
                    } else if (metoda.equals("podziel")) {
                        try {
                            if (liczba2 != 0) {
                                System.out.println(calculator.divideTwoNumbers(liczba1, liczba2));
                            } else throw new IllegalArgumentException();
                        } catch (IllegalArgumentException e) {
                            System.out.println(e + " Nie mozna dzielic przez 0");
                        }

                    } else throw new IllegalArgumentException();

                } catch (IllegalArgumentException e) {
                    System.out.println(e + " Nieznana metoda: " + metoda);
                }
            } catch (NumberFormatException nfe) {
                System.out.println("Musisz podac 2 liczby");
            }
        }

        /*System.out.println(calculator.addTwoNumbers(4,4));
        System.out.println(calculator.substractTwoNumbers(4,4));
        System.out.println(calculator.multiplyTwoNumbers(4,4));
        System.out.println(calculator.divideTwoNumbers(4,4));*/

    }
}
