package zadanie15;

public class Main2 {
    public static void main(String[] args) {
        // a
        Phone p = new Phone();
        p.call();
        p.call();// trwa połączenie
        p.disconnect();
        p.disconnect();//nie ma trwającego połączenia

        // b
        p.call("123");
        p.call();// trwa połączenie
        p.call("234");// trwa połączenie
        p.disconnect();
        p.disconnect(); //nie ma trwającego połączenia
    }
}