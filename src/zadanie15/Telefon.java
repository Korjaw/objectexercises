package zadanie15;

public class Telefon {

    private Boolean czyDzwoni = false;
    private String numerTelefonu;
    private String zawartoscWyswietlacza;
    private String wybranyNumer;

    public Telefon() {
    }

    public void zadzwon(String wybranyNumer) {
        try {
            if (getCzyDzwoni() == false) {
                setCzyDzwoni(true);
                setWybranyNumer(wybranyNumer);
                setZawartoscWyswietlacza(getNumerTelefonu());
            } else throw new IllegalArgumentException();

        } catch (IllegalArgumentException e) {
            System.out.println(e + " Nie mozna wykonac drugiego polaczenia jesli juz dzwoni");
        }

    }

    public void rozlacz() {
        try {
            if (getCzyDzwoni() == true) {
                setCzyDzwoni(false);
            } else throw new IllegalArgumentException();
        } catch (IllegalArgumentException e) {
            System.out.println(e + " Brak rozmowy do rozlaczenia");
        }

    }

    public Boolean getCzyDzwoni() {
        return czyDzwoni;
    }

    public void setCzyDzwoni(Boolean czyDzwoni) {
        this.czyDzwoni = czyDzwoni;
    }

    public String getNumerTelefonu() {
        return numerTelefonu;
    }

    public void setNumerTelefonu(String numerTelefonu) {
        this.numerTelefonu = numerTelefonu;
    }

    public String getZawartoscWyswietlacza() {
        if (getCzyDzwoni()==false) {
            return "Pusty";
        } else {
            return "Rozmowa z " + getNumerTelefonu();
        }
    }

    public void setZawartoscWyswietlacza(String zawartoscWyswietlacza) {
        this.zawartoscWyswietlacza = zawartoscWyswietlacza;
    }

    public String getWybranyNumer() {
        return wybranyNumer;
    }

    public void setWybranyNumer(String wybranyNumer) {
        this.wybranyNumer = wybranyNumer;
    }

    @Override
    public String toString() {
        return "Telefon{" +
                "czyDzwoni=" + getCzyDzwoni() +
                ", numerTelefonu='" + getNumerTelefonu() + '\'' +
                ", zawartoscWyswietlacza='" + getZawartoscWyswietlacza() + '\'' +
                ", wybranyNumer='" + getWybranyNumer() + '\'' +
                '}';
    }
}
