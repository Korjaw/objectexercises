package zadanie13;

import java.util.ArrayList;
import java.util.List;

public class Student2 {
    private String name;
    private String surname;
    private int index;
    private List<Integer> grades = new ArrayList<>();

    public Student2() {
    }

    public Student2(String name, String surname, int index) {
        this.name = name;
        this.surname = surname;
        this.index = index;
    }

    public void addGrade(int grade) {
        // dodanie oceny do zbioru ocen studenta.
        grades.add(grade);
    }

    // metodę która zwraca true jeśli żadna z ocen w liście/tablicy
    // ocen nie jest 1 ani 2, oraz false w przeciwnym razie.
    public boolean didPass() {
        for (Integer grade : grades) {
            if (grade <= 2) {
                // jesli jest 1 lub 2 to zwracamy false (przerywamy petle)
                return false;
            }
        }
        // jesli nie ma zadnej 1 lub 2 to petla sie skonczy i zwracamy true
        return true;
    }

    // metodę do obliczania średniej
    public double getAverage() {
        // nie było w treści
        // zabezpieczeneie przed brakiem ocen
        if (grades.size() == 0) {
            throw new ArithmeticException("Brak ocen!");
        }
        double sum = 0.0;
        for (Integer grade : grades) {
            sum += grade;
        }
        // liczenie sredniej
        return (sum / grades.size());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}