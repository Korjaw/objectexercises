package zadanie13;

import java.util.ArrayList;
import java.util.List;

public class Student {

    private int numerIndeksu;
    private String imie;
    private String nazwisko;
    private int sum = 0;
    List<Integer> listaOcen = null;

    public Student() {

    }

    public void srednia() {

      for(int i=0; i<listaOcen.size(); i++) {
            sum +=  listaOcen.get(i);
        }
        int srednia = sum/listaOcen.size();
        System.out.println(srednia);
    }

    public boolean check() {

        for(int i=0; i<listaOcen.size(); i++) {
            if (listaOcen.get(i)== 1 || listaOcen.get(i)==2) {
                return false;
            }
        }
        return true;
    }

    public int getNumerIndeksu() {
        return numerIndeksu;
    }

    public void setNumerIndeksu(int numerIndeksu) {
        this.numerIndeksu = numerIndeksu;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public List<Integer> getListaOcen() {
        return listaOcen;
    }

    public void setListaOcen(List<Integer> listaOcen) {
        this.listaOcen = listaOcen;
    }

    @Override
    public String toString() {
        return "Student{" +
                "numerIndeksu=" + getNumerIndeksu() +
                ", imie='" + getImie() + '\'' +
                ", nazwisko='" + getNazwisko() + '\'' +
                ", listaOcen=" + getListaOcen() +
                '}';
    }
}
