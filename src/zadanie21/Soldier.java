package zadanie21;

public class Soldier extends Citizen {

    public Soldier(String imie) {
        super(imie);
    }

    @Override
    public boolean canVote() {
        return true;
    }

    @Override
    public String toString() {
        return "Soldier{}";
    }
}
