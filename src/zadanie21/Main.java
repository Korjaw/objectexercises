package zadanie21;

public class Main {

    public static void main(String[] args) {

        Town city = new Town();

        city.addCitizen(new King("krol1"));
        city.addCitizen(new Townsman("townsman1"));
        city.addCitizen(new Townsman("townsman2"));
        city.addCitizen(new Soldier("sol1"));
        city.addCitizen(new Soldier("sol2"));
        city.addCitizen(new Peasant("peasant1"));
        city.addCitizen(new Peasant("peasant2"));
        city.addCitizen(new King("krol2"));

        System.out.println("Ilosc obywateli mogacych glosowac:" + city.howManyCanVote());
        System.out.println("Kto moze glosowac: " + city.whoCanVote());
        city.whoCanVote2();
    }
}
