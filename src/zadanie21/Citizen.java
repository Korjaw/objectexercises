package zadanie21;

public abstract class Citizen {

    protected String imie;

    public Citizen(String imie) {
        this.imie = imie;
    }

    public abstract boolean canVote();

    @Override
    public String toString() {
        return "Citizen{" +
                "imie='" + imie + '\'' +
                '}';
    }
}
