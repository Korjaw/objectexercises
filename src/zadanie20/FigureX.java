package zadanie20;

public abstract class FigureX {
    public abstract double obliczObwod();
    public abstract double obliczPole();
}