package zadanie20;

public class SquareX extends FigureX {
    private double a;

    public SquareX(double a) {
        this.a = a;
    }

    @Override
    public double obliczObwod() {
        return 4 * a;
    }

    @Override
    public double obliczPole() {
        return a * a;
    }
}