package zadanie19;

public class Father extends FamilyMember {

    public Father(String name) {
        super(name);
    }

    @Override
    public void introduce() {
        System.out.println("I am father..." + name);
    }
}