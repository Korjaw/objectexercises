package zadanie19;

public class Mother extends FamilyMember {

    public Mother(String name) {
        super(name);
    }

    @Override
    public void introduce() {
        System.out.println("I am mother..." + name);
    }
    public void ogladajTV(){
        System.out.println("Oglądam telewizje");
    }
}