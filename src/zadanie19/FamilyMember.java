package zadanie19;

// Utwórz klasę FamilyMember z polem name i metodą introduce()
// która wypisuje komunikat "I am just a simple family member".
public abstract class FamilyMember {
    protected String name;

    public FamilyMember(String name) {
        this.name = name;
    }

    public void introduce(){
        System.out.println("I am just a simple family member");
    }
}