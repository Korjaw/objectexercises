package zadanie2;

public class TeddyBear {

    private String name;

    public TeddyBear(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "TeddyBear{" +
                "name='" + name + '\'' +
                '}';
    }
}
