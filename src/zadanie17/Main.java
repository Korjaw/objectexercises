package zadanie17;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Generator g = new Generator(4, 7);
        Scanner sc = new Scanner(System.in);

        boolean isWorking = true;
        while (isWorking) {
            String slowo = sc.next();

            switch (slowo) {
                case "next":
                    System.out.println(g.generate());
                    break;
                case "range":
                    System.out.println(g.generateInRange());
                    break;
                case "char":
                    System.out.println(g.generateChar());
                    break;
                default:
                    System.out.println("Unknown command");
            }
        }
    }
}