package zadanie17;

public class Generator {
    private int number;
    private int rangeNumber;
    private int evenNumber;
    private int mulNumber;
    private boolean booleanResult = false;

    private int charNumber;

    // do podpunktu b
    private int lowRange;
    private int highRange;

    // do podpunktu b
    public Generator(int lowRange, int highRange) {
        this.lowRange = lowRange;
        this.highRange = highRange;
    }

    // Random generator = new Random();
    // int liczba = generator.nextInt()

    //a) Kolejnych liczb.
    public int generate() {
        // po zwróceniu jakiejs liczby number sie inkrementuje
        return number++;
    }

    //b) Kolejnych liczb w podanym przedziale. (przedział podawany w konstruktorze)
    public int generateInRange() {
        int rangeSize = highRange - lowRange + 1;
        return lowRange + (rangeNumber++ % rangeSize);
    }

    //c) Kolejnych liczb parzystych.
    public int generateEven() {
        return (evenNumber += 2);
    }

    //d) Kolejnych wielokrotności liczby podanej w parametrze.
    public int generateMul(int number) {
        return number * mulNumber++;
    }

    //e) Występujących na przemian wartości true, false.
    public boolean generateBoolean() {
        return (booleanResult = !booleanResult);
    }

    //h) Kolejnych liter alfabetu (znaków unicode od a do z).
    public char generateChar() {
        // 97 - początek
        // 122 - koniec
        // charNumber++ - kolejny znak
        // 'z' - 'a' - długość przedziału z którego robimy modulo
        // dzięki dodaniu 1 do przedziału będzie możliwość 'wygenerowania'
        // litery 'z'
        return (char) ('a' + (charNumber++ % ('z' - 'a' + 1)));
    }

}