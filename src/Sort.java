import java.util.Arrays;

public class Sort {
    public static void main(String[] args) {
        sort(new int[]{1,  5 ,2, 3, 4,5,6,7} );
    }

    public static void sort(int[] array) {
        mergeSort(array, 0, array.length - 1);
    }

    public static void mergeSort(int[] tablica, int first, int last) {
        if (first != last) {
            int middle = ((last - first) / 2) + first;
            // split lewej
            mergeSort(tablica, first, middle);

//            System.out.println("first: " + first + ", middle: " + middle + ", last:" + last);
            // split prawej
            mergeSort(tablica, middle + 1, last);

            // laczenie (merge) obu stron
            merge(tablica, first, middle, last);
        }
    }

    public static void merge(int[] tablica, int first, int middle, int last) {
        int[] tmp = new int[tablica.length];
        for (int i = 0; i < tablica.length; i++) {
            tmp[i] = tablica[i];
        }
        System.out.println(Arrays.toString(tmp));

        int i1 = first;
        int i2 = middle + 1;
        int indeks = first;

        // (i1 <= middle) - dopĂłki sÄ… elementy po lewej stronie
        // (i2 <= last) - dopĂłki sÄ… elementy po prawej stronie
        while ((i1 <= middle) && (i2 <= last)) {
            if (tmp[i1] > tmp[i2]) {
                tablica[indeks] = tmp[i2];
                i2++;
            } else {
                tablica[indeks] = tmp[i1];
                i1++;
            }
            indeks++;
        }

        for (int i = i1; i <= middle; i++) {
            tablica[indeks++] = tmp[i];
        }
        for (int i = i2; i <= last; i++) {
            tablica[indeks++] = tmp[i];
        }
    }
}