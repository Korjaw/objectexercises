package zadanie14;

public class Main {

    public static void main(String[] args) {

        Paczka paczka = new Paczka();

        System.out.println(paczka);

        paczka.wyslij();

        paczka.setCzyJestZawartosc(true);
        paczka.wyslij();

        paczka.setOdbiorca("Jan");
        paczka.wyslij();

        System.out.println(paczka);

        paczka.wyslijPolecony();
        paczka.setNadawca("Janusz");
        paczka.wyslijPolecony();


    }
}
