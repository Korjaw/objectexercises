package zadanie14;

public class Paczka {

    private String odbiorca = "brak";
    private String nadawca = "brak";
    private Boolean czyZostalaWyslana = false;
    private Boolean czyJestZawartosc = false;

    public Paczka() {
    }

    public Paczka(String odbiorca, Boolean czyJestZawartosc) {
        this.odbiorca = odbiorca;
        this.czyJestZawartosc = czyJestZawartosc;
    }

    public void wyslij() {
        try {
            if (getCzyJestZawartosc() == true) {

                try {
                    if (getOdbiorca().equals("brak")) throw new IllegalArgumentException();
                    else {
                        System.out.println("Nadano paczke");
                        setCzyZostalaWyslana(true);
                    }

                } catch (IllegalArgumentException e) {
                    System.out.println(e + " Brak odbiorcy");
                }

            } else throw new IllegalArgumentException();
        } catch (IllegalArgumentException e) {
            System.out.println(e + " Brak zawartosci");
        }
    }

    public void wyslijPolecony() {
        try {
            if (getCzyJestZawartosc() == true) {

                try {
                    if (getOdbiorca().equals("brak")) throw new IllegalArgumentException();
                    else {
                        try {
                            if (getNadawca().equals("brak")) throw new IllegalArgumentException();
                            else {
                                System.out.println("Nadano paczke");
                                setCzyZostalaWyslana(true);
                            }
                        } catch (IllegalArgumentException e) {
                            System.out.println(e + " Brak nadawcy");
                        }
                    }

                } catch (IllegalArgumentException e) {
                    System.out.println(e + " Brak odbiorcy");
                }

            }
        } catch (IllegalArgumentException e) {
            System.out.println(e + " Brak zawartosci");
        }
    }

    public String getOdbiorca() {
        return odbiorca;
    }

    public void setOdbiorca(String odbiorca) {
        this.odbiorca = odbiorca;
    }

    public String getNadawca() {
        return nadawca;
    }

    public void setNadawca(String nadawca) {
        this.nadawca = nadawca;
    }

    public Boolean getCzyZostalaWyslana() {
        return czyZostalaWyslana;
    }

    public void setCzyZostalaWyslana(Boolean czyZostalaWyslana) {
        this.czyZostalaWyslana = czyZostalaWyslana;
    }

    public Boolean getCzyJestZawartosc() {
        return czyJestZawartosc;
    }

    public void setCzyJestZawartosc(Boolean czyJestZawartosc) {
        this.czyJestZawartosc = czyJestZawartosc;
    }

    @Override
    public String toString() {
        return "Paczka{" +
                "odbiorca='" + getOdbiorca() + '\'' +
                ", nadawca='" + getNadawca() + '\'' +
                ", czyZostalaWyslana=" + getCzyZostalaWyslana() +
                ", czyJestZawartosc=" + getCzyJestZawartosc() +
                '}';
    }
}
