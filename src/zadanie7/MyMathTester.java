package zadanie7;

public class MyMathTester {

    public static void main(String[] args) {

        MyMath a = new MyMath(0);

        System.out.println(a.abs(-1));
        System.out.println(a.abs(1));

        System.out.println(a.abs(2.0 ));
        System.out.println(a.abs(-2.0 ));

        System.out.println(a.pow(2,1));
        System.out.println(a.pow(9,2));

        System.out.println(a.pow(2.5,1));
        System.out.println(a.pow(9.3,2));

    }
}
