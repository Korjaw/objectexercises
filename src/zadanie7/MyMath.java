package zadanie7;

public class MyMath {

    private int a;
    private int b;

    public MyMath(int a) {
        this.a = a;
    }

    public int abs(int a) {
        if (a > 0) {
            a = a;
        }
        if (a < 0) {
            a = a * (-1);

        }
        return a;
    }


    public double abs(double a) {
        if (a>0) {
            a=a;
        }
        if (a<0) {
            a=a*(-1);

        }
        return a;
    }

    public int pow(int a, int b) {
        int wynik = 1;

        for (int i=1; i<=b; i++) {
            wynik*=a;
        }
        return wynik;
    }

    public double pow(double a, int b) {
        double wynik = 1.0;

        for (int i=1; i<=b; i++) {
            wynik*=a;
        }
        return wynik;
    }
}
